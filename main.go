package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"strings"
	"time"
)

var (
	port = flag.String("port", ":2223", "")
)

func handleConnection(c net.Conn, msgchan chan<- string) {
	defer c.Close()
	fmt.Printf("Connection from %v established.\n", c.RemoteAddr())
	c.SetReadDeadline(time.Now().Add(time.Minute * 1))
	buf := make([]byte, 4096)
	for {
		n, err := c.Read(buf)
		if (err != nil) || (n == 0) {
			c.Close()
			break
		}
		msgchan <- string(buf[0:n])
	}
	time.Sleep(150 * time.Millisecond)
	fmt.Printf("Connection from %v closed.\n", c.RemoteAddr())
}

func printMessages(msgchan <-chan string) {
	for {
		msg := strings.TrimSpace(<-msgchan)
		log.Printf("Data: %s\n", msg)
	}
}

func main() {
	flag.Parse()
	ln, err := net.Listen("tcp", *port)
	if err != nil {
		panic(err)
	}
	msgchan := make(chan string)
	go printMessages(msgchan)
	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}
		go handleConnection(conn, msgchan)
	}
}
